import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userUrl:string='http://localhost:8081/mservice1'
  constructor(private httpClient:HttpClient) { }
  
 
  getAllUsers(){
   return this.httpClient.get(this.userUrl);
 }
}
