
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseCapitalComponent } from './component/base-capital/base-capital.component';
import { CalendrierComponent } from './component/calendrier/calendrier.component';




import { EmployeeComponent } from './component/employee/employee.component';
import { EmploymentHistoryComponent } from './component/employment-history/employment-history.component';
import { FinancialComponent } from './component/financial/financial.component';
import { HomeComponent } from './component/home/home.component';
import { LicenseComponent } from './component/license/license.component';
import { LoginComponent } from './component/login/login.component';
import { MyProfileComponent } from './component/my-profile/my-profile.component';
import { PersonalParticularsComponent } from './component/personal-particulars/personal-particulars.component';
import { PersonalComponent } from './component/personal/personal.component';
import { PlatformComponent } from './component/platform/platform.component';

import { TrialBalanceComponent } from './component/trial-balance/trial-balance.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path : '',
    component: HomeComponent
  },
  {
    path : 'financial',
    component: FinancialComponent
  },
 
  {
    path : 'employee',
    component: EmployeeComponent
  },
  {
    path : 'license',
    component: LicenseComponent
  },
  {
    path : 'personal',
    component: PersonalComponent
  },
  {
    path : 'platform',
    component: PlatformComponent
  },
  {
    path : 'base',
    component: BaseCapitalComponent
  },

  {
    path : 'trial',
    component: TrialBalanceComponent
  },

  {
    path : 'profile',
    component: MyProfileComponent
  },
  {
    path : 'ps',
    component: PersonalParticularsComponent
  },
  {
    path : 'eh',
    component: EmploymentHistoryComponent
  },
  {
    path : 'date',
    component: CalendrierComponent
  },
 {
    path : 'date',
    component: CalendrierComponent
  },
 


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
