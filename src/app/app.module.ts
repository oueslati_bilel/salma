import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule  } from "@angular/common/http";

import { MatSliderModule } from '@angular/material/slider';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { EmployeeComponent } from './component/employee/employee.component';
import { PersonalComponent } from './component/personal/personal.component';
import { PlatformComponent } from './component/platform/platform.component';
import { LicenseComponent } from './component/license/license.component';
import { FinancialComponent } from './component/financial/financial.component';
import { BaseCapitalComponent } from './component/base-capital/base-capital.component';
import { TrialBalanceComponent } from './component/trial-balance/trial-balance.component';

import { MyProfileComponent } from './component/my-profile/my-profile.component';
import { PersonalParticularsComponent } from './component/personal-particulars/personal-particulars.component';
import { EmploymentHistoryComponent } from './component/employment-history/employment-history.component';
import { CalendrierComponent } from './component/calendrier/calendrier.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatListModule} from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon'
import {SidenavService} from "./Shared/sidenav.service";
import {MatDialogModule} from "@angular/material/dialog";
import { TrialPopupComponent } from './PopUp/trial-popup/trial-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SidebarComponent,
    LoginComponent,
    EmployeeComponent,
    PersonalComponent,
    PlatformComponent,
    LicenseComponent,
    FinancialComponent,
    BaseCapitalComponent,
    TrialBalanceComponent,
    MyProfileComponent,
    PersonalParticularsComponent,
    EmploymentHistoryComponent,
    CalendrierComponent,
    TrialPopupComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSliderModule,
    AngularSvgIconModule.forRoot(),
    MatFormFieldModule,
    MatDatepickerModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatDialogModule




   ],
  providers: [SidenavService ],
  bootstrap: [AppComponent]
})
export class AppModule { }


