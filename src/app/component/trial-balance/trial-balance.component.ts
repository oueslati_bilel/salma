import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {TrialPopupComponent} from "../../PopUp/trial-popup/trial-popup.component";
//import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { ModalService } from 'ng-bootstrap-modal';

@Component({
  selector: 'app-trial-balance',
  templateUrl: './trial-balance.component.html',
  styleUrls: ['./trial-balance.component.scss']
})
export class TrialBalanceComponent implements OnInit {

  constructor( private dialogRef:MatDialog) { }

  ngOnInit(): void {
  }

  openDialog(){
    this.dialogRef.open(TrialPopupComponent)
  }

}
