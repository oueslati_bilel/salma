import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalParticularsComponent } from './personal-particulars.component';

describe('PersonalParticularsComponent', () => {
  let component: PersonalParticularsComponent;
  let fixture: ComponentFixture<PersonalParticularsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalParticularsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalParticularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
